
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { GlobalesService } from '../globales.service';
//***
import { MenuController } from '@ionic/angular';
import { AtaskService } from '../atask.service';
import { Storage } from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ASTROURL } from '../constants';
import { DatePipe } from '@angular/common';
import { interval } from 'rxjs';

import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';


@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private datePipe: DatePipe,
      public globales: GlobalesService,
      private atask: AtaskService,
      private menu: MenuController,
      private storage: Storage,
      private http :HttpClient,
      public loadingController: LoadingController,
      public toastController: ToastController
      ) { }

    contentzoom: boolean=false;
    user:string;
    token:string;
    private intervalohora = interval(1000);
    datetimef1 :string;
    title: string = 'Tarea Planeada';
    tstask:any;
    tsnow:any;
    lat: number;
    lng: number;
    id= "";
    tasklist: any;
    task:any;
    mapOptions: any;
    markerOptions: any = {position: null, map: null, title: null};
    marker: any;
    mapCenter = {lat: 7, lng: -74};
    location={lat:7,lng:-74}
    stylemap=[
              {
                  "featureType": "all",
                  "elementType": "all",
                  "stylers": [
                      {
                          "hue": "#e7ecf0"
                      }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "road",
                  "elementType": "all",
                  "stylers": [
                      {
                          "saturation": -70
                      }
                  ]
              },
              {
                  "featureType": "transit",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      },
                      {
                          "saturation": -60
                      }
                  ]
              }
          ]

    timecheckin(){
      var totalminutes=Math.floor(((new Date(this.tsnow).getTime())-(new Date(this.tstask).getTime()))/60000);
      if (totalminutes<0) var horas=(Math.floor(totalminutes/60))+1; else var horas=Math.floor(totalminutes/60);
      var minutos=(Math.floor(((totalminutes/60)-horas)*60))
      var stringtime=horas+"h :"+minutos+"m";

        if(totalminutes>-10)
        {
          (<HTMLInputElement> document.getElementById(this.id)).disabled = false;
        }

      return {stringtime:stringtime,totalminutes:totalminutes}
    }

//notacion URL
//ids[]=xxxID1xxxx&ids[]=xxxID2xxx
    activetask(task_id){
      this.storage.get('activetask'+task_id).then(task=>{
        if (task==null) {
          var taskact = this.globales.tasklist["Active"];
          var taskpla = this.globales.tasklist["Planned"];
          var object= taskact.concat(taskpla);
            for (let item of object ) {
              if (item.id==task_id) {
                this.task=item;
                this.storage.set("activetask"+task_id,this.task).then(ok=>{
                  this.router.navigate(["/active/",task_id,true]);
                },err=>{});
              }
            }
          }  else {
            this.router.navigate(['/active/',task_id,false]);
          }
        },err=>console.log("taskpage error"))
      }



    checkin() {
      this.activeLoading();
      const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
              'X-Practitioner-Email': this.user,
              'X-Practitioner-Token': this.token
            })
          };
      let ids_tourl="";
      for (let id of this.task.procedure){
        ids_tourl=ids_tourl+"ids[]="+id.id+"&";
      }
      let ts: Number = ((new Date).getTime())/1000;
      this.http
      .get(ASTROURL+"/check_in.json?"+ids_tourl+"ts="+ts, httpOptions)
      .subscribe((res:any)=>{
        this.activetask(this.id);
      },err=>{this.presentToast(err);});
    }

    async activeLoading() {
      const loading = await this.loadingController.create({
        message: 'Activando...',
        duration: 2000
      });
      await loading.present();
      const { role, data } = await loading.onDidDismiss();
    }
    async presentToast(error) {
    const toast = await this.toastController.create({
      message: 'Error iniciando actividad:',
      duration: 5000
    });
    toast.present();
  }




    wraptap() {
      if (this.contentzoom==false) this.contentzoom=true; else this.contentzoom=false;
    }

    zoomstyle(){
      if (this.contentzoom==false)
        return {map:{height: '40vh'}, content:{height: '40vh'}};
        else
        return {map:{height: '0vh'}, content:{height: '80vh'}};
    }

    starttimer(){
      let sub=this.intervalohora.subscribe(res=>{
        this.tsnow=new Date();
      })
    }

    ngOnInit() {
      this.menu.enable(true, 'main');
      this.id = this.route.snapshot.paramMap.get('id');
      this.storage.get('tasklist').then(tasklist=>{
        if (tasklist!=null) {
          for (let status of ["Planned","Active","Finished","Cancelled"]){
            for (let item of tasklist[status]) {
              if (item.id==this.id) {
                this.tstask=item.datetimeas;
                this.task=item;
                console.log("XD"+item.procedure[0].dosage);
                this.globales.task=item;
                console.log("this",this);
                console.log("task encontrado!! [planned]");}
            }
          }
        }
      },(err)=>{console.log("error storage token:",err)});
      this.storage.get('login').then(login=>{
        this.user=login.user;
        this.token=login.token;
      })
      this.starttimer();
    }

    ionViewWillEnter(){

    }
}
