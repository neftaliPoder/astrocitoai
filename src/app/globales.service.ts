import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Storage } from '@ionic/storage';
import {Ctask} from './ctask';
//import {Task} from './task';
import {Coordenada} from './coordenada';

@Injectable({
  providedIn: 'root'
})
export class GlobalesService {
  constructor(
  private datePipe: DatePipe,
  private storage: Storage
  ) {

  }
  public menu: boolean;
  public user: string;
  public name: string;
  public surname: string;
  public token: string;
  public onduty: boolean;
  public loginpage: boolean;
  public location={latitude:4.6803404,longitude:-74.0663091};
  public currentuser :string;
  public currenttoken :string;
  public prueba: string;
  public batt: number;
  public tasklist={"Planned":[],"Active":[],"Finished":[],"Cancelled":[],"nulo":[]}
  public apigooglemaps = "AIzaSyBq9fZJXiE1Qi-Cym6hkoowwRzwiHeKBzg"
  public loincs={};
  public encounteritemcodes= {};
  public currentactivetask: any;
  public task ={careplan:String,
              datetime:String,
              datetimeas: new Date().toString(),
              distance: Number,
              id: String,
              patient_address: String,
              patient_city: String,
              patient_contact: String,
              patient_email: String,
              patient_gender: String,
              patient_idtype: String,
              patient_lat: Number,
              patient_lon: Number,
              patient_month_age: Number,
              patient_name: String,
              patient_nid: String,
              patient_phone: String,
              patient_phone_contact: String,
              patient_years_age: Number,
              procedure: [{medication:"",concentration:"",administration:"",dilution:"",infusion_time:"",cold_chain:false, dosage:""}],
              masa: String
            }
  //public task:Task;
  public activetasks = []


/*
  taskparse(tipo) {
    for (task in this.task) {
      return task[tipo]
    }
  }*/
        buscaactive(id){
          var res=false;
          this.storage.get('tasklist').then(
            tasklist=>{
            if (tasklist!=null) {
              this.tasklist=tasklist;
              for (let item of this.tasklist["Active"]) {
                if (item.id==id) {
                  this.task=item;
                  //this.tomarTarea(item);
                  console.log("this",this);
                  console.log("task encontrado!! [active]");}
                  res=true
              }
            }
          },(err)=>{console.log("error storage token:",err)});
          return res;
        }

      /*  tomarTarea(item)
        {
          this.task=new Task();
          this.task.setCareplan(item.careplan);
          this.task.setDateTime(item.datetime);
          this.task.setDateTimeAs(item.datetimeas);
          this.task.setDistance(item.distance);
          this.task.setId(item.id);
          this.task.setPatientAddress(item.patient_address);
          this.task.setPatientCity(item.patient_city);
          this.task.setPatientContact(item.patient_contact);
          this.task.setPatientEmail(item.patient_email);
          this.task.setPatientGender(item.patient_gender);
          this.task.setPatientIdType(item.patient_idtype);
          this.task.setPatientLocation(new Coordenada(item.patient_lat,item.patient_lon));
          this.task.setPatientMonthAge(item.patient_month_age);
          this.task.setPatientName(item.patient_name);
          this.task.setPatientIdNum(item.patient_nid);
          this.task.setPatientPhone(item.patient_phone);
          this.task.setPatientPhoneContact(item.patient_phone_contact);
          this.task.setPatientYearsAge(item.patient_years_age);
          this.task.setPatientMonthAge(item.patient_month_age);
        }*/

        tasklistdate(date,status) {
            let res=[];
            let fechaq:string;
            for (let item of this.tasklist[status]) {
              fechaq=this.datePipe.transform(item.datetimeas,"yyyy-MM-dd");
              if (date==fechaq) res.push(item);
            }
            return res;
        }


/*
        put(clave:string,valor:any) {
          this.storage.set(clave,valor).then(ok=>{console.log("guardado:",clave,valor)},err=>console.log("[native storage put error]",err));
        }

        get(clave:string) {
          this.storage.get(clave).then(res=>{return res},err=>{console.log("[native storage get error]",err)});
        }
*/
    }
