import { TestBed } from '@angular/core/testing';

import { BackgeoService } from './backgeo.service';

describe('BackgeoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BackgeoService = TestBed.get(BackgeoService);
    expect(service).toBeTruthy();
  });
});
