import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

//custom
import { HttpClient } from '@angular/common/http';
import { HttpParams } from "@angular/common/http";
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalesService } from './globales.service';
import { MenuController } from '@ionic/angular';
import { BackgeoService } from './backgeo.service';
import { WatchbattService } from './watchbatt.service';
import { AtaskService } from './atask.service';
import { AlertController } from '@ionic/angular';

//import { BackgroundMode } from '@ionic-native/background-mode/ngx';


import { ASTROURL } from './constants';
import { ASTROSECRET } from './constants';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
  //  private backgroundMode: BackgroundMode,
    private menu: MenuController,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private http: HttpClient,
    public globales: GlobalesService,
    private backgeo: BackgeoService,
    private watchbatt :WatchbattService,
    private atask: AtaskService,
    private alertController: AlertController
  ) {
    this.initializeApp();
  }
  private loginpage :boolean
  private onduty :boolean

  name:string;
  surname:string;

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.overlaysWebView(false);
      this.statusBar.backgroundColorByHexString('#428BCA');
      this.splashScreen.hide();
    });
  }

/*
  desactivarsesion(){
    //this.storage.get('onduty').then(onduty=>this.onduty=onduty},err=>{console.log("error storage onduty:",err)});
    //this.storage.set('token',res.token).then(ok=>{},err=>console.log("errstorage",err));
    this.backgeo.stopBackgroundGeolocation();
    this.globales.onduty=false;
    this.atask.stop();
  }*/

    cerrarsesion(){
      this.backgeo.stopBackgroundGeolocation();
      this.globales.onduty=false;
      this.globales.currentuser ="";
      this.globales.currenttoken="";
      this.storage.clear().then(ok=>{this.router.navigate(['/home']);});
    }

    /*activarsesion(){
      this.globales.onduty=true;
      this.atask.once();
      this.backgeo.startBackgroundGeolocation();
    }*/

    async cerrarsesionalerta() {
      const alert = await this.alertController.create({
        header: 'Cerrar Sesion',
        message: 'Esta seguro que desea cerrar la sesión?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              this.alertController.dismiss();
            }
          }, {
            text: 'Si',
            handler: () => {
              this.cerrarsesion();
            }
          }
        ]
      });
      await alert.present()
    }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.storage.get('login').then(login=>{
      if (login==null)
        this.router.navigate(['/home']);
      else {
        this.name=login.name;
        this.surname=login.surname;
        this.globales.currenttoken=login.token;
        this.globales.currentuser=login.user;};
    },(err)=>console.log("[error storage token:]",err));
  }
}
