import { Component, OnInit } from '@angular/core';
import { GlobalesService } from '../globales.service';
import { MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ModalController } from '@ionic/angular';
import { FirmamodalPage } from './firmamodal/firmamodal.page'

@Component({
  selector: 'app-updatesignature',
  templateUrl: './updatesignature.page.html',
  styleUrls: ['./updatesignature.page.scss'],
})
export class UpdatesignaturePage implements OnInit {

login={name:"",surname:"",nid:"",idtype:"",signature:""};

  constructor(
    private storage: Storage,
    private globales: GlobalesService,
    private menu: MenuController,
    private router: Router,
    private modalController: ModalController
  ) { }

  async cambiarfirma() {
      const modal=await this.modalController.create({
            component: FirmamodalPage,
            componentProps: {
               login: this.login,
            }
      });

      modal.onDidDismiss().then((detail: any) => {
         if (detail !== null) {
           console.log('The result:', detail.data);
         }
      });

      await modal.present();
  }

  ionViewWillEnter() {
    this.globales.loginpage=false;
    this.modalController.dismiss();
    //this.menu.enable(true, 'main');
    this.storage.get('login').then(login=>{
      this.login=login;
      //this.signaturePad.fromDataURL(this.login.signature);
    })
  }

  ngOnInit() {
    this.menu.enable(true, 'main');
    this.modalController.dismiss();

  }

}
