import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatesignaturePage } from './updatesignature.page';

describe('UpdatesignaturePage', () => {
  let component: UpdatesignaturePage;
  let fixture: ComponentFixture<UpdatesignaturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatesignaturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatesignaturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
