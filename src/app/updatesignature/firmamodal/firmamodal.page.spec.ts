import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FirmamodalPage } from './firmamodal.page';

describe('FirmamodalPage', () => {
  let component: FirmamodalPage;
  let fixture: ComponentFixture<FirmamodalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FirmamodalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirmamodalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
