import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { ASTROURL } from '../../constants';
import { GlobalesService } from '../../globales.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-firmamodal',
  templateUrl: './firmamodal.page.html',
  styleUrls: ['./firmamodal.page.scss'],
})


export class FirmamodalPage implements OnInit {

  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private globales: GlobalesService,
    private storage: Storage
  ) { }

  login:any;

  @ViewChild(SignaturePad,{static: false }) signaturePad: SignaturePad;

  signaturePadOptions = {
    'minWidth': 1,
    'canvasWidth': 350,
    'canvasHeight': 200
  };

  enviarfirma(){
    var json={signature:this.signaturePad.toDataURL()}
    var url1=ASTROURL+"/signature_practitioner?practitioner_email="+this.globales.currentuser+"&practitioner_token="+this.globales.currenttoken;
    var reqx = new XMLHttpRequest();
    if (this.signaturePad.isEmpty()==true)
      { console.log("caja vacia")}
      else {
          reqx.onreadystatechange = function () {
          if (reqx.readyState == 4 && reqx.status == 201) {
            console.log("Para enviar");
          }}
          reqx.open("POST", url1, true);
          reqx.setRequestHeader("Content-type", "multipart/form-data");
          reqx.send('json='+JSON.stringify(json));
          console.log("[firma enviada!]")
          this.login.signature=json.signature
          this.storage.set('login',this.login).then(ok=>this.closeModal())
      }
  }



  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }

  ngOnInit() {
  }

}
