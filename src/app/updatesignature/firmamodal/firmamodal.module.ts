import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SignaturePadModule } from 'angular2-signaturepad';

import { FirmamodalPage } from './firmamodal.page';

const routes: Routes = [
  {
    path: '',
    component: FirmamodalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    SignaturePadModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [FirmamodalPage]
})
export class FirmamodalPageModule {}
