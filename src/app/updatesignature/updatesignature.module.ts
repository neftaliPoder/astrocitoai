import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { SignaturePadModule } from 'angular2-signaturepad';
import { UpdatesignaturePage } from './updatesignature.page';
import { ModalController } from '@ionic/angular';
//import { FirmamodalComponent } from './firmamodal/firmamodal.component'


const routes: Routes = [
  {
    path: '',
    component: UpdatesignaturePage
  }
];

@NgModule({
  imports: [
    SignaturePadModule,
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UpdatesignaturePage]
})
export class UpdatesignaturePageModule {}
