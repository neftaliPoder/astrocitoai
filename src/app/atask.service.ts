import { Injectable } from '@angular/core';
import { GlobalesService } from './globales.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpParams } from "@angular/common/http";
import { ASTROURL } from './constants';
import { ASTROSECRET } from './constants';
import { interval } from 'rxjs';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AtaskService {

  constructor(
    private globales: GlobalesService,
    private http: HttpClient,
    private storage: Storage,
    public  toastController: ToastController
  ) { }

    async once(dateNow:string){
        this.storage.get('login').then(login=>{
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
              'X-Practitioner-Email': login.user,
              'X-Practitioner-Token': login.token
            })
          };
        this.http
        .get(ASTROURL+"/get_tasks.json?datetoday="+dateNow,httpOptions)
        .subscribe((res:any)=>{
          console.log("get atask ",res)
          this.globales.tasklist=res;
          this.storage.set('tasklist',res).then(ok=>{},err=>console.log("errstorage",err));
        },err=>this.httperror(err));
        this.http
        .get(ASTROURL+"/get_loinc.json",httpOptions)
        .subscribe((res:any)=>{
          console.log("get estaticos ok",res);
          this.storage.set('staticcodes',res).then(ok=>{},err=>console.log("staticcodes storage err",err));
        },err=>console.log("error getloinc_atask",err));
      })
    }

    httperror(err){
      console.log("[atask http error]", err);
      var toast =  this.toastController.create({
      message: 'Error accediendo al servidor',
      duration: 5000
      });
      toast.then(tostada => {tostada.present()}, err=>{console.log("errordetotstada",err)})
    }
  }
