import { TestBed } from '@angular/core/testing';

import { AtaskService } from './atask.service';

describe('AtaskService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AtaskService = TestBed.get(AtaskService);
    expect(service).toBeTruthy();
  });
});
