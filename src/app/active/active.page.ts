import { Component, OnInit, ViewChild, Pipe, PipeTransform } from '@angular/core';
import { GetloincService } from '../getloinc.service';
import { GlobalesService } from '../globales.service';
import { AtaskService } from '../atask.service';
import { AlertController, IonSlides, ToastController} from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { SignaturePad} from 'angular2-signaturepad/signature-pad';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { ASTROURL } from '../constants';
import { Storage } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { Ctask } from '../ctask'
import { LoincTagComponent} from '../loinc-tag/loinc-tag.component'
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-active',
  templateUrl: './active.page.html',
  styleUrls: ['./active.page.scss'],
})
export class ActivePage implements OnInit {

  constructor(
    private getloinc: GetloincService,
    public globales: GlobalesService,
    private route: ActivatedRoute,
    private router: Router,
    private speechRecognition: SpeechRecognition,
    private http: HttpClient,
    private storage: Storage,
    private datePipe: DatePipe,
    private atask: AtaskService,
    public toastController: ToastController,
    public loadingController: LoadingController
    ) { }
    @ViewChild(IonSlides,{static: false }) slides: IonSlides;
    @ViewChild(SignaturePad,{static: false }) signaturePad: SignaturePad;

    public slideOpts = {
            effect: 'cube',
            initialSlide: 0
            };
    public res=[];
    id="";
    maxdate=this.datePipe.transform(new Date(),'yyyy-MM-dd');
    list_loincs=[];
    list_encounteritems=[];
    data_loincs={};
    data_encounteritems={};
    task={procedure:[]};
    oki: boolean;
    novelty: string;
    login={name:"",surname:"",nid:"",idtyp:"",signature:""};




  signaturePadOptions = {
    'minWidth': 1,
    'canvasWidth': 350,
    'canvasHeight': 300
  };

guardarstorage()
{
  this.storage.set('activefields'+this.id,{data_loincs:this.data_loincs,data_encounteritems:this.data_encounteritems}).then(guardado=>console.log("guardado!",this.data_loincs,this.data_encounteritems)).catch((error)=>{console.log("[error active loinc storage]")});
}

drawComplete()
{
  this.storage.set('patientSignature'+this.id,{signature:this.signaturePad.toDataURL()}).then(guardado=>console.log("guardada firma del paciente!")).catch((error)=>{console.log("[error active patientSignature storage]")});
}

borrarSignaturePad()
{
  this.signaturePad.clear();
  this.drawComplete();
}

spanbool(item){
  if (this.data_encounteritems[item]==true)
  return ["spanbool","spanbooltrue"];
  else return ["spanbool","spanboolfalse"];
}

hidehack(){
  if (this.task['procedure'].length==0) return false; else return true;
}


spanoki(){
  if (this.oki==true)
  return ["spanbool","spanbooltrue"];
  else return ["spanbool","spanboolfalse"];
}

sino(bool){
  if (bool==true) return "SI"; else return "NO";
}


crearjson(status)
        {
          this.activeLoading();
          let ids=[];
          for (let id of this.task.procedure){
            ids.push(id.id);
          }
          let form={};
          let form2={};
          form=this.data_loincs;
          form2=this.data_encounteritems;
          let ts: Number = ((new Date).getTime())/1000;
          let id=this.id;
          //console.log({id:id,ts:ts,form:form,signature:this.signaturePad.toDataURL()});
          if  (status=="completed"){
            this.enviar({ids:ids,ts:ts,status:"Finished",form_observations:form,form_encounter:form2,novelty:"Finalizado con Éxito",signature:this.signaturePad.toDataURL()})
          } else if (status=="cancelled") {
            this.enviar({ids:ids,ts:ts,status:"Cancelled",form_observations:form,form_encounter:form2,novelty:this.novelty,signature:this.signaturePad.toDataURL()})
          }
        }

    enviar(json){
      this.storage.get('login').then(login=>{
      var url1=ASTROURL+"/check_out_encounter.json";
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
          'X-Practitioner-Email': login.user,
          'X-Practitioner-Token': login.token
        })
      };
      this.http.post(url1,'json='+encodeURIComponent(JSON.stringify(json)) , httpOptions)
        .subscribe(data => {
          console.log("httpok!:",data);
          this.storage.remove('activefields'+this.id).then(ok=>console.log("borrado!!!")).catch((error)=>{console.log("error storage remove")});
          this.storage.remove('activetask'+this.id).then(ok=>console.log("borrado!!!")).catch((error)=>{console.log("error storage remove")});
          this.storage.remove('patientSignature'+this.id).then(ok=>console.log("borrado!!!")).catch((error)=>{console.log("error storage remove")});
          this.router.navigate(['/tasklist']);
          this.presentToast("Actividad enviada con éxito.");
         }, error => {
          console.log("error http:", error);
          this.presentToast("Error envío de actividad.");
        });
      })
    }

    async activeLoading() {
      const loading = await this.loadingController.create({
        message: 'Enviando...',
        duration: 2000
      });
      await loading.present();
      const { role, data } = await loading.onDidDismiss();
    }

    async presentToast(mess) {
    const toast = await this.toastController.create({
      message: mess,
      duration: 3000
    });
    toast.present();
  }


    lockswipe(){
      this.slides.lockSwipes(true);
    }

    unlockswipe(){
      this.slides.lockSwipes(false);
    }

    iniciardictado(){
      let speechoptions={
          language:"es-CO",
        }
      this.speechRecognition.requestPermission()
          .then(
            (ok) => console.log('speechGranted'),
            (err) => console.log('speechDenied')
          )
      this.speechRecognition.startListening(speechoptions)
        .subscribe(
          (matches: Array<string>) => {
            console.log(matches);
            this.data_encounteritems['I00039-6']=matches[0];
          },
          (onerror) => console.log('error:', onerror)
        )
    }

    initializeStorage()
    {
      /*this.storage.get('activeNow'+this.id).then(actiNow=>{*/ this.storage.get('activetask'+this.id).then(task=>{
        //this.task=actiNow.activetask;
        this.task=task;
        for (let concept of this.list_loincs) {
          if (concept["concept_type"]=="Bl")
            this.data_loincs[concept["concept_code_id"]]=false;
           else
            this.data_loincs[concept["concept_code_id"]]="";
          };
          for (let conceptencounter of this.list_encounteritems) {
            if (conceptencounter["concept_type"]=="Bl"){
              this.data_encounteritems[conceptencounter["encounter_item_code_id"]]=false;}
            else if(conceptencounter["encounter_item_code_id"]=="I00045-9") {
              this.data_encounteritems["I00045-9"]={}
              for (let procedure of this.task.procedure){
                this.data_encounteritems["I00045-9"][procedure.id]={reginvima:"",lote:"",fecha:""}
              }
            }
             else
              this.data_encounteritems[conceptencounter["encounter_item_code_id"]]="";
          }
          console.log("vacio por nuevo loincs:",this.data_loincs);
          console.log("vacio por nuevo encounteritem:",this.data_encounteritems);
          this.storage.set('activefields'+this.id,{data_loincs:this.data_loincs,data_encounteritems:this.data_encounteritems}).then(ok=>{}).catch((error)=>{console.log("error [active] set storage")});
        }).catch((error)=>{console.log("error [active] get storage task")});
    }

    ngOnInit() {
      this.globales.menu=false;
      this.storage.get('staticcodes').then(staticcodes=>{
          this.oki=true;
          console.log("staticcodes ",staticcodes)
          console.log("[todo active]",this);
          this.list_loincs=staticcodes.loincs;
          this.list_encounteritems=staticcodes.encounteritemcodes;
          var nuevo:string
          this.id = this.route.snapshot.paramMap.get('id');
          nuevo = this.route.snapshot.paramMap.get('nuevo');
          if (nuevo == "true")
          {
            console.log("nuevoactive")
            this.initializeStorage();
          } else
          {
            console.log("viejoactive")
            //this.storage.get('activeNow'+this.id).then(actiNow=>{this.task=actiNow.activetask;this.data_loincs=actiNow.activefields.data_loincs;this.data_encounteritems=actiNow.activefields.data_encounteritems;this.signaturePad.fromDataURL(actiNow.patientSignature.signature,{width:this.signaturePadOptions.canvasWidth, height:this.signaturePadOptions.canvasHeight})}).catch((error)=>{console.log("XDDDDDD"+error)});
            this.storage.get('activetask'+this.id).then(task=>{this.task=task}).catch((error)=>{console.log("error [active] get storage task..."+error);this.initializeStorage()});
            this.storage.get('activefields'+this.id).then(activefields=>{this.data_loincs=activefields.data_loincs;this.data_encounteritems=activefields.data_encounteritems}).catch((error)=>{console.log("error [active] get storage loinc");this.initializeStorage()});
            this.storage.get('patientSignature'+this.id).then(patientsign=>{this.signaturePad.fromDataURL(patientsign.signature,{width:this.signaturePadOptions.canvasWidth, height:this.signaturePadOptions.canvasHeight})}).catch((error)=>{console.log("error [active] get storage patientSignature")});
          }
      }).catch((error)=>{console.log("error get storage",error)});
      this.storage.get('login').then(login=>{
        this.login=login;
        //this.signaturePad.fromDataURL(this.login.signature);
      });
    }
}
