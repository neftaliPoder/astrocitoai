import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivePage } from './active.page';

describe('ActivePage', () => {
  let component: ActivePage;
  let fixture: ComponentFixture<ActivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
