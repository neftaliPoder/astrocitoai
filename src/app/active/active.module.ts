import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { IonicModule } from '@ionic/angular';
import { ActivePage } from './active.page';
import { SignaturePadModule } from 'angular2-signaturepad';
import { LoincTagComponent } from '../loinc-tag/loinc-tag.component';


const routes: Routes = [
  {
    path: '',
    component: ActivePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    SignaturePadModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ActivePage,LoincTagComponent],
  providers: []
})
export class ActivePageModule {}
