import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpParams } from "@angular/common/http";
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ASTROURL } from '../constants';
import { ASTROSECRET } from '../constants';
import { ToastController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { GlobalesService } from '../globales.service';
import { BackgeoService } from '../backgeo.service';
import { DatePipe } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AtaskService } from '../atask.service';

@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.page.html',
  styleUrls: ['./tasklist.page.scss'],
})
export class TasklistPage implements OnInit {
  alertOt=false;
  constructor (
    private storage: Storage,
    private router: Router,
    private http: HttpClient,
    private menu: MenuController,
    private toastController: ToastController,
    public globales: GlobalesService,
    private datePipe: DatePipe,
    private atask :AtaskService,
    private backgeo :BackgeoService
    ) {
    }

    clickactive(task_id){
      this.storage.get('activetask'+task_id).then(task=>{
        if (task==null) {
            for (let item of this.globales.tasklist["Active"]) {
              if (item.id==task_id) {
                console.log("encontrado active usado sin storage");
                this.task=item;
                this.storage.set("activetask"+task_id,this.task).then(ok=>{
                  this.router.navigate(["/active/",task_id,true]);
                },err=>{});
              }
            }
          }  else {
            this.router.navigate(['/active/',task_id,false]);
          }
        },err=>console.log("errorgetstorage"))
      }

    taskdate: any
    fechaq: string
    tasklist:any
    task:any

    menost(datetimeas,tipo,id)
    {
      var ya:any=new Date();
      var ant:any=new Date(datetimeas);
      var timein:string="";
      switch(tipo)
      {
        case 1:
          var rtime:number=Math.floor(((ya-ant)/1000));
        break;
        case 2:
          var rtime:number=Math.floor(((ant-ya)/1000));
        break;
        default:
          return ""+":"+"";
        break;
      }

      if(rtime>=0)
      {
        timein="";
      }else
      {
        timein="-";
      }

      var sec_num:number=Math.abs(rtime);
      var horas:number=Math.floor(sec_num/3600);
      var minutos:number=Math.floor((sec_num-(horas*3600))/60);
      var H:string=""+horas;
      var M:string=""+minutos;

      if((((horas*100)+minutos)>10)&&(timein=="-")&&(tipo==2))
      {
        document.getElementById(id+"control").style.backgroundColor='#c70f02';
      }
      if(horas<10)
      {
        H="0"+horas;
      }
      if(minutos<10)
      {
        M="0"+minutos;
      }
      timein+=(H+":"+M);
      return timein;
    }

    reloaddatepicker() {
      this.fechaq=this.datePipe.transform(this.fechaq,"yyyy-MM-dd");
      this.storage.set('fechaq',this.fechaq).then(ok=>{});
      this.atask.once(this.datePipe.transform(this.fechaq,"yyyy/MM/dd"));
    }

    ionViewWillEnter() {
      this.backgeo.startBackgroundGeolocation()
      this.menu.enable(true, 'main');
      var now1=Date.now();
      this.storage.get('login').then(login=>{
        if (login!=null){
          this.globales.name=login.name;
          this.globales.surname=login.surname;
          if((this.fechaq==null)||(this.fechaq==""))
          {
            this.atask.once(this.datePipe.transform(now1,"yyyy/MM/dd"));
          }else
          {
            this.atask.once(this.datePipe.transform(this.fechaq,"yyyy/MM/dd"));

          }

        } else {
          console.log('storage login nulo')
          this.router.navigate(['/home']);
        }
      })
      this.storage.get('fechaq').then(fechaq=>{
        if ((fechaq==null)||(fechaq=="")){
          this.fechaq=this.datePipe.transform(now1,"yyyy-MM-dd");
        } else this.fechaq=fechaq;
      });
    }

  refresc(event){
    console.log("refresc!!!!");
    this.atask.once(this.datePipe.transform(this.fechaq,"yyyy/MM/dd")).then((ok)=>event.target.complete());
    console.log("atask reload");
  }

  httperror(err){
    console.log("[tasklist http error]", err);
    var toast =  this.toastController.create({
    message: 'Error accediendo al servidor',
    duration: 5000
    });
    toast.then(tostada => {tostada.present()}, err=>{console.log("errordetotstada",err)})
  }

  ngOnInit()
  {

  }

}
