import { TestBed } from '@angular/core/testing';

import { WatchbattService } from './watchbatt.service';

describe('WatchbattService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WatchbattService = TestBed.get(WatchbattService);
    expect(service).toBeTruthy();
  });
});
