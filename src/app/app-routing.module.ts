import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: './home/home.module#HomePageModule' },
  { path: 'tasklist', loadChildren: './tasklist/tasklist.module#TasklistPageModule' },
  { path: 'task/:id', loadChildren: './task/task.module#TaskPageModule' },
  { path: 'active/:id/:nuevo', loadChildren: './active/active.module#ActivePageModule' },
  { path: 'updatesignature', loadChildren: './updatesignature/updatesignature.module#UpdatesignaturePageModule'},
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]

})
export class AppRoutingModule { }
