import { Injectable } from '@angular/core';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import { GlobalesService } from './globales.service';


@Injectable({
  providedIn: 'root'
})
export class WatchbattService {

  constructor(
    public globales: GlobalesService,
    public batterystatus: BatteryStatus
  ) { }

  start() {
  const subscription = this.batterystatus.onChange().subscribe(status => {
   console.log("battinfo:",status.level, status.isPlugged);
   this.globales.batt=status.level*100
      });
  }

  stop() {
  }


}
