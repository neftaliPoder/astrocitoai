import { Component, OnInit } from '@angular/core';
//custom
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ASTROURL } from '../constants';
import { ASTROSECRET } from '../constants';
import { ToastController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { GlobalesService } from '../globales.service';
import { BackgeoService } from '../backgeo.service';
import { AtaskService } from '../atask.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    public user:string
    public password:string

    constructor(
      private storage: Storage,
      private router: Router,
      private menu: MenuController,
      public globales :GlobalesService,
      public backgeo: BackgeoService,
      public atask: AtaskService,
      public auth: AuthService,
    ) {
      this.menu.close();
      this.globales.loginpage=true;
      this.globales.onduty = false;
    }

    clickId(){
      console.log("usr,psw",this.user,this.password)
      this.auth.loginclick(this.user,this.password);
    };

    ionViewWillEnter(){
      this.menu.close();
      this.menu.enable (false, 'main' );
      this.storage.get('login').then(login=>{
        if (login!=null){
          this.router.navigate(['/tasklist']);
        } else {
          this.globales.onduty = true;
          this.backgeo.stopBackgroundGeolocation();
          this.user="";
          this.password="";
        }
      })
    }

    ngOnInit() {
      this.menu.close();
    }
}
