import { Injectable } from '@angular/core';
import { ASTROURL } from './constants';
import { ASTROSECRET } from './constants';
import { HttpClient , HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { GlobalesService } from './globales.service';



@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public globales: GlobalesService,
    private router: Router,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
  ) { }

  public user:string
  public password:string

  loginclick(email:string, password:string){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8',
        'X-Practitioner-Email': ASTROSECRET.user,
        'X-Practitioner-Token': ASTROSECRET.token
      })
    };

    this.user=email;
    this.http
    .get(ASTROURL+"/get_token.json?user_email="+email+"&user_password="+password,httpOptions)
    .subscribe((res:any)=>{this.login(res)},err=>this.loginerr(err));
  }

  login(res) {
    console.log("resultado login : ",res);
    this.storage.set('login',{user: this.user, token: res.token, name: res.name, surname:res.surname, signature: res.signature, idtype: res.idtype, nid:res.nid}).then(ok=>{
      this.globales.user=this.user;
      this.globales.token=res.token;
      this.globales.currentuser=this.user;
      this.globales.currenttoken=res.token;
      this.globales.name=res.name;
      this.globales.surname=res.surname;
      this.router.navigate(['/tasklist'])});
  }

  loginerr(err){
    console.log("errornumero",err.status)
    if (err.status==404) {
        var toast =  this.toastController.create({
        message: 'Usuario no encontrado',
        duration: 5000
      });
      toast.then(tostada => {tostada.present()}, err=>{console.log("errordetotstada",err)})
    } else if (err.status==401) {
        var toast =  this.toastController.create({
        message: 'Contraseña Incorrecta',
        duration: 5000
      });
      toast.then(tostada => {tostada.present()}, err=>{console.log("errordetotstada",err)})
    } else {
        var toast =  this.toastController.create({
        message: 'Error del Servidor Autenticación',
        duration: 5000
      });
      toast.then(tostada => {tostada.present()}, err=>{console.log("errordetotstada",err)})
    }
  }

}
