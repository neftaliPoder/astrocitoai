import { Injectable } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse, BackgroundGeolocationEvents } from '@ionic-native/background-geolocation/ngx';
import { GlobalesService } from './globales.service';
import { WatchbattService } from './watchbatt.service';
import { ASTROURL } from './constants';
import { ASTROSECRET } from './constants';



@Injectable({
  providedIn: 'root'
})
export class BackgeoService {

  constructor(
    public globales: GlobalesService,
    public backgroundGeolocation: BackgroundGeolocation,
    private watchbatt :WatchbattService
  ) { }


  startBackgroundGeolocation() {
    //this.watchbatt.start()
    const config: BackgroundGeolocationConfig = {
      url: ASTROURL+'/location.json',//?practitioner_email='+this.globales.currentuser+'&practitioner_token='+this.globales.currenttoken,
      //syncUrl: ASTROURL+'/location.json',//?practitioner_email='+this.globales.currentuser+'&practitioner_token='+this.globales.currenttoken,
      desiredAccuracy: 11,
      stationaryRadius: 1,
      notificationTitle: "Innovar Salud",
      notificationText: "Modo disponibilidad :HABILITADO",
      distanceFilter: 1,
      interval: 180000,
      fastestInterval: 180000,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
      httpHeaders: {
        'X-Practitioner-Email': this.globales.currentuser,
        'X-Practitioner-Token': this.globales.currenttoken,
        'Access-Control-Allow-Origin': '*'
      },
      postTemplate: {
        lat: '@latitude',
        lon: '@longitude',
        provider: '@provider',
        ts: '@time',
      }
      };
      this.backgroundGeolocation.configure(config).then(() => {
        this.backgroundGeolocation
          .on(BackgroundGeolocationEvents.location)
          .subscribe((location: BackgroundGeolocationResponse) => {
            console.log("locacion",location);
            this.globales.location=location
            //console.log("batt:", this.globales.batt)
            //this.sendpos(location);
            // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
            // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
            // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
          });
      });
      console.log("[inicio geolocalización background]")
      this.backgroundGeolocation.start();
    }


    stopBackgroundGeolocation() {
      console.log("[fin geolocalización background]")
      this.backgroundGeolocation.stop();
    }

}
