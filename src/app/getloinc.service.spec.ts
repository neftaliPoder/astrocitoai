import { TestBed } from '@angular/core/testing';

import { GetloincService } from './getloinc.service';

describe('GetloincService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetloincService = TestBed.get(GetloincService);
    expect(service).toBeTruthy();
  });
});
