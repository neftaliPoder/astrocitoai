import { Component, Input, OnInit } from '@angular/core';
//import { GlobalesService } from '../globales.service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'loinc-tag',
  templateUrl: './loinc-tag.component.html',
  styleUrls: ['./loinc-tag.component.scss'],
})
export class LoincTagComponent implements OnInit {
  constructor(
    //private globales:GlobalesService,
    private storage:Storage
  ) { }
  @Input() code_id: string;
  name: string;
  question:string
  description: string;
  scale: string;
  unit: string;
  type:string

  ngOnInit() {
    this.storage.get("loincs").then(loincs=>{
      console.log("init elemet child dentro de storage")
      var found=false
      for (let loinc of loincs){
        if (loinc['concept_code_id']==this.code_id){
          found=true
          this.name=loinc['name']
          this.question=loinc['question']
          this.description=loinc['description']
          this.scale=loinc['scale']
          this.unit=loinc['unit']
          this.type=loinc['concept_type']
        }
      }
    },err=>console.log("error get storage child element loinc",err))


  }

}
