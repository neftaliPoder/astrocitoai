import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoincTagComponent } from './loinc-tag.component';

describe('LoincTagComponent', () => {
  let component: LoincTagComponent;
  let fixture: ComponentFixture<LoincTagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoincTagComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoincTagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
