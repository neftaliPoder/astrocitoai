import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { BackgroundGeolocation } from "@ionic-native/background-geolocation/ngx";

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//CustomAstrocito
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { GlobalesService } from './globales.service';
import { BackgeoService } from './backgeo.service';
//import { LoincTagComponent} from './loinc-tag/loinc-tag.component';
import { BatteryStatus } from '@ionic-native/battery-status/ngx';
import { WatchbattService } from './watchbatt.service';
import { AtaskService } from './atask.service';
import { GetloincService } from './getloinc.service';
import { DatePipe } from '@angular/common';
import { SignaturePadModule } from 'angular2-signaturepad';
import { AgmCoreModule } from '@agm/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition/ngx';
import { FirmamodalPageModule } from './updatesignature/firmamodal/firmamodal.module'


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    FormsModule,
    SignaturePadModule,
    AgmCoreModule.forRoot({apiKey: 'AIzaSyBq9fZJXiE1Qi-Cym6hkoowwRzwiHeKBzg'}),
    HttpClientModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FirmamodalPageModule,
    IonicStorageModule.forRoot()],
  providers: [
    BackgroundGeolocation,
    BackgeoService,
    GlobalesService,
    SpeechRecognition,
    StatusBar,
    BatteryStatus,
    DatePipe,
    SplashScreen,
    AtaskService,
    WatchbattService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
